import * as Koa from "koa";
import * as koaBody from "koa-body";

import * as logger from "koa-logger";
import * as json from "koa-json";

import rootRouter from "./routes/root";
import usersRouter from "./routes/users";
import itemsRouter from "./routes/items";
import locationsRouter from "./routes/locations";
import actionsRouter from "./routes/actions";
import permsRouter from "./routes/perms";
import authRouter from "./routes/auth";

const app = new Koa();

app.use(koaBody());

app.use(async (ctx, next) => {
  try {
    ctx.request.body = JSON.parse(ctx.request.body);
  } catch (e) {}

  await next();
});

app.use(json());
app.use(logger());

app.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Set-Cookie, Authorization');
  if(ctx.method == 'OPTIONS') {
    ctx.status = 200;
  } else {
    await next();
  }
});

app.use(rootRouter.routes()).use(rootRouter.allowedMethods());
app.use(usersRouter.routes()).use(usersRouter.allowedMethods());
app.use(itemsRouter.routes()).use(itemsRouter.allowedMethods());
app.use(locationsRouter.routes()).use(locationsRouter.allowedMethods());
app.use(actionsRouter.routes()).use(actionsRouter.allowedMethods());
app.use(permsRouter.routes()).use(permsRouter.allowedMethods());
app.use(authRouter.routes()).use(authRouter.allowedMethods());

export default app;

import * as Router from "koa-router";
import {ResponseBody, ResponseStatus} from "../interfaces";

const router = new Router()

router.get("/", async (ctx) => {
  ctx.body = {
    status: ResponseStatus.ok,
    data: { test: "SERVICE WORKS" }
  } as ResponseBody;
});

export default router;

import * as Router from "koa-router";
import * as sql from "mssql";
import config from "../db"
import {ResponseBody, ResponseStatus} from "../interfaces";
import checkTokenMiddleware from "./checkTokenMiddleware";

const router = new Router({ prefix: '/perms' });

router.use(checkTokenMiddleware);

router.get("/", async (ctx, next) => {
	let pool = await sql.connect(config);
	let { recordset: perms } = await pool.request().query(`
		select 
			upr_id as id,
			upr_nazwa as name
		from 
			uprawnienia
		where
			upr_dost = 1;
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: perms
	} as ResponseBody;

	pool.close();
	sql.close();
	
	await next();
});

export default router;

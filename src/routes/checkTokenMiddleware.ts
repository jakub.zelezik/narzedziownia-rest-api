import verifyToken from "../verifyToken";
import {ResponseBody, ResponseStatus} from "../interfaces";

async function checkTokenMiddleware(ctx, next) {
    const tokenData = await verifyToken(ctx.request.header.authorization);

    if (tokenData != null) {
        ctx.state = tokenData;

        ctx.request.body = ctx.request.body.data;

        await next();
    } else {
        ctx.status = 401;
        ctx.body = {
            status: ResponseStatus.authError
        } as ResponseBody;
    }
}

export default checkTokenMiddleware;

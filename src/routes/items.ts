import * as Router from "koa-router";
import * as sql from "mssql";

import config from "../db"
import parse_error from "../parseError";

import {ResponseBody, ResponseStatus} from "../interfaces";
import * as Joi from "joi";
import checkTokenMiddleware from "./checkTokenMiddleware";
import QueryFilter from "../queryFilter";

const router = new Router({ prefix: '/items' });

router.use(checkTokenMiddleware);

router.get("/", async (ctx) => {
	const filter = ctx.request.query.filter;

	let pool = await sql.connect(config);
	let { recordset: items } = await new QueryFilter().query(`
		select 
			wypos_id as id,
			wypos_name as name,
			wypos_opis as description,
			wypos_datadod as creation_date,
			wypos_numerser as serial_number,
			wypos_numerkat as part_number
		from 
			wyposazenie
		where
			%%FILTER_RULES%%;
	`).addCustomFilter('hold_by', (dict, item): string => {
		console.log(item);
		return `wypos_id=ANY(
			select
    			o.wypos_id
			from
    			dbo.operacje o
			where
    			o.oper_id = (select MAX(o2.oper_id) from dbo.operacje o2 where o2.wypos_id=o.wypos_id) and o.os_idprzyjm=@${item.field}_filter
    	)`;
	}).addCustomFilter('location', (dict, item): string => {
		console.log(item);
		return `wypos_id=ANY(
			select
    			o.wypos_id
			from
    			dbo.operacje o
			where
    			o.oper_id = (select MAX(o2.oper_id) from dbo.operacje o2 where o2.wypos_id=o.wypos_id) and o.lok_id=@${item.field}_filter
    	)`;
	}).filter(filter, {
		user_id: 'user_id',
		location_id: 'location_id'
	}).call(pool.request());

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: items
	} as ResponseBody;

	pool.close();
	sql.close();
});

router.get("/:id", async (ctx) => {
	const id = ctx.params.id;
	let pool = await sql.connect(config);
	let { recordset: items } = await pool.request()
	.input('id', sql.Int, id).query(`
		select 
			wypos_id as id,
			wypos_name as name,
			wypos_opis as description,
			wypos_datadod as creation_date,
			wypos_numerser as serial_number,
			wypos_numerkat as part_number
		from 
			wyposazenie
		where
			wypos_id = @id;
	`);

	if(items.length == 0) {
		ctx.status = 404;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: [{
				field: null,
				error: `Wyposażenie o id = ${id} nie istnieje!`
			}]
		} as ResponseBody;
	} else {
		let [item] = items;

		/*let { recordset: history } = await pool.request().input('id', sql.Int, id).query(`
			select
				oper_id as id,

			from
				operacje o, 
			where
				o.os_id = @id;
		`);*/

		ctx.status = 200;
		ctx.body = {
			status: ResponseStatus.ok,
			data: item
		} as ResponseBody;
	}

	pool.close();
	sql.close();
});

router.post("/new", async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		name: Joi.string()
			.min(3)
			.max(50)
			.required(),
		part_number: Joi.string()
			.min(3)
			.max(50)
			.required(),
		serial_number: Joi.string()
			.min(3)
			.max(50)
			.required(),
		description: Joi.string()
			.min(3)
			.max(300)
			.required(),
		location_id: Joi.number()
			.min(1)
			.required(),
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 402;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parse_error(e)
		} as ResponseBody;

		return;
	}

	await sql.connect(config);
	let pool = await sql.connect(config);

	const transaction = pool.transaction()
	await new Promise(resolve => transaction.begin(resolve));

	const {  
		name,
		part_number,
		serial_number,
		description,
		location_id
	} = body;

	const user_id = ctx.state.id;
	const operation_description = 'Wprowadzenie wyposażenia na stan';
	const operation_confirmed = true;

	if(ctx.state.perm_id < 2) {
		ctx.status = 401;
		ctx.body = {
			status: ResponseStatus.authError,
			errors: [{
				field: null,
				error: 'Nie masz wystarczających uprawnień aby wykonać tą operację'
			}]
		} as ResponseBody;

		return;
	}

	const {recordset: [{wypos_id}]} = await transaction.request()
		.input('name', sql.NVarChar(50), name)
		.input('part_number', sql.NVarChar(50), part_number)
		.input('serial_number', sql.NVarChar(50), serial_number)
		.input('description', sql.NVarChar(300), description)
		.query(`
			insert into wyposazenie (
				wypos_name,
				wypos_opis,
				wypos_numerser,
				wypos_numerkat
			) values (
				@name,
				@description,
				@serial_number,
				@part_number
			); select SCOPE_IDENTITY() as wypos_id;
	`);

	await transaction.request()
		.input('location_id', sql.NVarChar(50), location_id)
		.input('user_id', sql.NVarChar(50), user_id)
		.input('wypos_id', sql.NVarChar(300), wypos_id)
		.input('operation_description', sql.NVarChar(50), operation_description)
		.input('operation_confirmed', sql.NVarChar(300), operation_confirmed)
		.query(`
			insert into operacje (
				oper_opis,
				wypos_id,
				lok_id,
				os_idprzyjm,
				oper_potw
			) values (
				@operation_description,
				@wypos_id,
				@location_id,
				@user_id,
				@operation_confirmed
			);
	`);

	transaction.commit();

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok
	} as ResponseBody;

	sql.close();
});

router.post("/edit/description", async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		description: Joi.string()
			.min(3)
			.max(300)
			.required(),
		id: Joi.number()
			.min(1)
			.required(),
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 402;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parse_error(e)
		} as ResponseBody;

		return;
	}

	let pool = await sql.connect(config);
	let { recordset: items } = await pool.request()
		.input('id', sql.Int, body.id)
		.input('description', sql.NVarChar(300), body.description)
		.query(`
			update 
				wyposazenie
			set 
    			wypos_opis = @description
			where 
    			wypos_id = @id;
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: items
	} as ResponseBody;

	pool.close();
	sql.close();
});

export default router;

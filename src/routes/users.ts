import * as Router from "koa-router";
import * as sql from "mssql";

import config from "../db";

import * as Joi from "joi";
import {NewUserData, ResponseBody, ResponseStatus} from "../interfaces";
import parseError from "../parseError";
import checkTokenMiddleware from "./checkTokenMiddleware";

function makeid(length) {
	var result           = '';
	var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for ( var i = 0; i < length; i++ ) {
		 result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

const router = new Router({ prefix: '/users' });

router.use(checkTokenMiddleware);

router.get("/", async (ctx) => {
	let pool = await sql.connect(config);
	let { recordset: users } = await pool.request().query(`
		select 
			o.os_id as id,
			o.os_imie as name,
			o.os_nazwisko as surname,
			o.os_email as email,
			u.upr_nazwa as perm
		from 
			osoby o inner join uprawnienia u on(o.upr_id = u.upr_id);
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: users
	} as ResponseBody;

	pool.close();
	sql.close();
});

router.get("/:id", async (ctx) => {
	const id = ctx.params.id;
	let pool = await sql.connect(config);

	let { recordset: users } = await pool.request()
		.input('id', sql.Int, id).query(`
		select
			o.os_id as id,
			o.os_imie as name,
			o.os_nazwisko as surname,
			o.os_email as email,
			u.upr_nazwa as perm	
		from
			osoby o inner join uprawnienia u on(o.upr_id = u.upr_id)
		where
			o.os_id = @id;
	`);

	if(users.length == 0) {
		ctx.status = 404;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: [{
				field: null,
				error: 'Użytkownik o podanym id nie istnieje!'
			}]
		} as ResponseBody;
	} else {
		let [user] = users;

		/*let { recordset: history } = await pool.request().input('id', sql.Int, id).query(`
			select
				oper_id as id,

			from
				operacje o, 
			where
				o.os_id = @id;
		`);*/
		
		ctx.status = 200;
		ctx.body = {
			status: ResponseStatus.ok,
			data: user
		} as ResponseBody;
	}

	pool.close();
	sql.close();
});

router.post("/new", async (ctx) => {

	let { request: { body: data }} = ctx;

	const validator = Joi.object({
		name:	Joi.string()
			.min(3)
			.max(50)
			.required(),
		surname: Joi.string()
			.min(3)
			.max(50)
			.required(),
		email: Joi.string()
			.email()
			.required(),
		perm_id: Joi.number()
			.min(1)
			.less(ctx.state.perm_id)
			.required(),
		password: Joi.string()
			.min(6)
			.max(256)
			.required(),
	});

	try {
		data = Joi.attempt(data, validator, { abortEarly: false });
	} catch (e) {
		ctx.status = 406;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parseError(e)
		} as ResponseBody

		return;
	}

	let pool = await sql.connect(config);

	const {  
		name,
		surname,
		email,
		perm_id,
		password
	} = data as NewUserData;

	await pool.request()
		.input('name', sql.NVarChar(50), name)
		.input('surname', sql.NVarChar(50), surname)
		.input('email', sql.NVarChar(50), email)
		.input('perm_id', sql.Int, perm_id)
		.input('password', sql.NVarChar(256), password)
		.input('token', sql.NVarChar(50), makeid(50))
		.query(`
			insert into osoby (
				os_imie,
				os_nazwisko,
				os_email,
				upr_id,
				os_haslo,
				os_token
			) values (
				@name,
				@surname,
				@email,
				@perm_id,
				HASHBYTES('SHA2_256', @password),
				@token
			);
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok
	} as ResponseBody;

	pool.close();
	sql.close();
});

export default router;

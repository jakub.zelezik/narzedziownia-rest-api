import * as Router from "koa-router";
import * as sql from "mssql";

import config from "../db"
import parse_error from "../parseError";

import * as Joi from "joi";
import {ResponseBody, ResponseStatus} from "../interfaces";
import checkTokenMiddleware from "./checkTokenMiddleware";

const router = new Router({ prefix: '/locations' });

router.use(checkTokenMiddleware);

router.get("/", async (ctx) => {
	let pool = await sql.connect(config);
	let { recordset: locations } = await pool.request().query(`
		select 
			l.lok_id as id,
			l.lok_nazwa as name,
			l.lok_ulica as street,
			l.lok_kodpocz as post_code,
			l.lok_city as city,
			l.osodp_id as manager_id,
			lt.loktyp_nazwa as type,
			os.os_imie as manager_name,
			os.os_nazwisko as manager_surname
		from 
			lokalizacja l inner join lokalizacja_typ lt on(l.loktyp_id = lt.loktyp_id) left join osoby os on(os.os_id = l.osodp_id) ;
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: locations
	} as ResponseBody;

	pool.close();
	sql.close();
});

router.get("/types", async (ctx) => {
	let pool = await sql.connect(config);
	let { recordset: types } = await pool.request().query(`
		select
			loktyp_id as id,
			loktyp_nazwa as name
		from
    		lokalizacja_typ;
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: types
	} as ResponseBody;

	pool.close();
	sql.close();
});

router.get("/:id", async (ctx, next) => {
	const id = ctx.params.id;
	let pool = await sql.connect(config);
	let { recordset: locations } = await pool.request()
		.input('id', sql.Int, id).query(`
		select 
			l.lok_id as id,
			l.lok_nazwa as name,
			l.lok_ulica as street,
			l.lok_kodpocz as post_code,
			l.lok_city as city,
			lt.loktyp_nazwa as type
		from 
			lokalizacja l inner join lokalizacja_typ lt on(l.loktyp_id = lt.loktyp_id)
		where
			l.lok_id = @id;
	`);

	if(locations.length == 0) {
		ctx.status = 404;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: [{
				field: null,
				error: `Lokalizacja o id = ${id} nie istnieje!`
			}]
		} as ResponseBody;
	} else {
		let [location] = locations;

		/*let { recordset: history } = await pool.request().input('id', sql.Int, id).query(`
			select
				oper_id as id,

			from
				operacje o, 
			where
				o.os_id = @id;
		`);*/
		
		ctx.status = 200;
		ctx.body = {
			status: ResponseStatus.ok,
			data: location
		} as ResponseBody;
	}

	pool.close();
	sql.close();
	
	await next();
});

router.post("/new", async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		name: Joi.string()
			.min(3)
			.max(100)
			.required(),
		type_id: Joi.number()
			.min(1)
			.required(),
		street: Joi.string()
			.min(3)
			.max(50)
			.required(),
		post_code: Joi.string()
			.pattern(/^\d{2}-\d{3}$/)
			.required(),
		city: Joi.string()
			.min(3)
			.max(50)
			.required(),
		user_id: Joi.number()
			.min(1)
			.optional(),
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 401;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parse_error(e)
		} as ResponseBody;

		return;
	}

	let pool = await sql.connect(config);

	const {  
		name,
		type_id,
		street,
		post_code,
		city
	} = body;

	if(ctx.state.perm_id < 2) {
		ctx.status = 401;
		ctx.body = {
			status: ResponseStatus.authError,
			errors: [{
				field: null,
				error: 'Nie masz wystarczających uprawnień aby wykonać tą operację'
			}]
		} as ResponseBody;

		return;
	}

	await pool.request()
		.input('name', sql.NVarChar(100), name)
		.input('type_id', sql.Int, type_id)
		.input('street', sql.NVarChar(50), street)
		.input('post_code', sql.NVarChar(50), post_code)
		.input('city', sql.NVarChar(50), city)
		.query(`
			insert into lokalizacja (
				lok_nazwa,
				lok_ulica,
				lok_kodpocz,
				lok_city,
				loktyp_id
			) values (
				@name,
				@street,
				@post_code,
				@city,
				@type_id
			);
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok
	} as ResponseBody;

	pool.close();
	sql.close();
});

router.post("/edit/manager", async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		user_id: Joi.number()
			.min(1)
			.required(),
		id: Joi.number()
			.min(1)
			.required(),
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 402;
		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parse_error(e)
		} as ResponseBody;

		return;
	}

	let pool = await sql.connect(config);
	let { recordset: items } = await pool.request()
		.input('id', sql.Int, body.id)
		.input('user_id', sql.Int, body.user_id)
		.query(`
			update 
				lokalizacja
			set 
    			osodp_id = @user_id
			where 
    			lok_id = @id;
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok,
		data: items
	} as ResponseBody;

	pool.close();
	sql.close();
});

export default router;

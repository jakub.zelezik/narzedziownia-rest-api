import * as Router from "koa-router";
import * as sql from "mssql";

import config from "../db";
import parse_error from "../parseError";

import * as Joi from "joi";
import checkTokenMiddleware from "./checkTokenMiddleware";
import {ResponseBody, ResponseStatus} from "../interfaces";
import QueryFilter from "../queryFilter";

const router = new Router({ prefix: '/actions' })

router.use(checkTokenMiddleware);

router.get("/", async (ctx) => {
	const filter = ctx.request.query.filter;

	let pool = await sql.connect(config);
	let { recordset: actions } = await new QueryFilter().query(`
	SELECT 
		o.oper_id as id,
		o.oper_opis as description,
		o.oper_potw as confirmed,
		o.oper_data as creation_date,
		os_w.os_id as from_id,
		os_w.os_imie as from_name,
		os_w.os_nazwisko as from_surname,
		os_p.os_id as to_id,
		os_p.os_imie as to_name,
		os_p.os_nazwisko as to_surname,
		l.lok_id as location_id,
		l.lok_nazwa as location_name,
		lt.loktyp_nazwa as location_type,
		w.wypos_id as item_id,
		w.wypos_name as item_name
	FROM
		operacje o 
		left join osoby os_p on(o.os_idprzyjm = os_p.os_id) 
		left join osoby os_w on(o.os_idwydaj = os_w.os_id) 
		left join lokalizacja l on(l.lok_id = o.lok_id) 
		left join wyposazenie w on(w.wypos_id = o.wypos_id)
		left join lokalizacja_typ lt on(l.loktyp_id = lt.loktyp_id)
	WHERE
		%%FILTER_RULES%%
	ORDER BY
		o.oper_id DESC;
	`).addCustomFilter('unaccepted', (dict, item) => {
		return `( o.oper_potw = 0 OR o.oper_potw IS NULL)`;
	}).filter(
		filter, {
			empty: 'empty',
			from_id: 'os_w.os_id',
			to_id: 'os_p.os_id',
			location_id: 'l.lok_id',
			item_id: 'w.wypos_id'
		}
	).call(pool.request());

	ctx.body = {
		status: ResponseStatus.ok,
		data: []
	} as ResponseBody;

	for(const action of actions){
		const from = action.from_id == undefined ? undefined : {
			id: action.from_id,
			name: action.from_name,
			surname: action.from_surname
		};
		const to = action.to_id == undefined ? undefined : {
			id: action.to_id,
			name: action.to_name,
			surname: action.to_surname
		};
		const location = {
			id: action.location_id,
			name: action.location_name,
			type: action.location_type
		};
		const item = {
			id: action.item_id,
			name: action.item_name
		};

		ctx.body.data.push({
			id: action.id,
			from,
			to,
			location,
			item,
			description: action.description,
			confirmed: action.confirmed,
			creation_date: action.creation_date
		});
	}

	pool.close();
	sql.close();
});

router.post("/new", async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		from: Joi.number()
			.min(1)
			.required(),
		to: Joi.number()
			.min(1)
			.required(),
		location: Joi.number()
			.min(1)
			.required(),
		item: Joi.number()
			.min(1)
			.required(),
		description: Joi.string()
			.min(3)
			.max(300)
			.required(),
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 401;

		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parse_error(e)
		} as ResponseBody;

		return;
	}

	let pool = await sql.connect(config);

	const {  
		from,
		to,
		location,
		item,
		description
	} = body;

	if(ctx.state.perm_id < 2) {
		ctx.status = 401;
		ctx.body = {
			status: ResponseStatus.authError,
			errors: [{
				field: null,
				error: 'Nie masz wystarczających uprawnień aby wykonać tą operację'
			}]
		} as ResponseBody;

		return;
	}

	await pool.request()
		.input('from', sql.Int, from)
		.input('to', sql.Int, to)
		.input('location', sql.Int, location)
		.input('item', sql.Int, item)
		.input('description', sql.NVarChar(300), description)
		.query(`
			insert into operacje (
				oper_opis,
				wypos_id,
				lok_id,
				os_idwydaj,
				os_idprzyjm
			) values (
				@description,
				@item,
				@location,
				@from,
				@to
			);
	`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok
	} as ResponseBody;

	pool.close();
	sql.close();
});

router.post('/accept', async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		id: Joi.number()
			.greater(0)
			.required()
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 401;

		ctx.body = {
			status: ResponseStatus.dataError,
			errors: parse_error(e)
		} as ResponseBody;

		return;
	}

	let pool = await sql.connect(config);

	await pool.request()
		.input('id', sql.Int, body.id)
		.input('user_id', sql.Int, ctx.state.id)
		.query(`
			update 
				operacje
			set 
    			oper_potw = 1
			where 
    			os_idprzyjm = @user_id and oper_id = @id;
		`);

	ctx.status = 200;
	ctx.body = {
		status: ResponseStatus.ok
	} as ResponseBody;

	pool.close();
	sql.close();
});
/*
router.get("/unaccepted", async (ctx) => {
	const user_id = 1;

	let pool = await sql.connect(config);
	let { recordset: actions } = await pool.request()
	.input('user_id', sql.Int, user_id)
	.query(`
	SELECT 
		o.oper_id as id,
		o.oper_opis as description,
		o.oper_potw as confirmed,
		o.oper_data as creation_date,
		os_w.os_id as from_id,
		os_w.os_imie as from_name,
		os_w.os_nazwisko as from_surname,
		os_p.os_id as to_id,
		os_p.os_imie as to_name,
		os_p.os_nazwisko as to_surname,
		l.lok_id as location_id,
		l.lok_nazwa as location_name,
		lt.loktyp_nazwa as location_type,
		w.wypos_id as item_id,
		w.wypos_name as item_name
	FROM
		operacje o 
		left join osoby os_p on(o.os_idprzyjm = os_p.os_id) 
		left join osoby os_w on(o.os_idwydaj = os_w.os_id) 
		left join lokalizacja l on(l.lok_id = o.lok_id) 
		left join wyposazenie w on(w.wypos_id = o.wypos_id)
		left join lokalizacja_typ lt on(l.loktyp_id = lt.loktyp_id)
	WHERE
		os_p.os_id = @user_id AND ( o.oper_potw = 0 OR o.oper_potw IS NULL);
	`);

	ctx.body = {
		status: ResponseStatus.ok,
		data: []
	} as ResponseBody;

	for(const action of actions){
		const from = action.from_id == undefined ? undefined : {
			id: action.from_id,
			name: action.from_name,
			surname: action.from_surname
		};
		const to = action.to_id == undefined ? undefined : {
			id: action.to_id,
			name: action.to_name,
			surname: action.to_surname
		};
		const location = {
			id: action.location_id,
			name: action.location_name,
			type: action.location_type
		};
		const item = {
			id: action.item_id,
			name: action.item_name
		};

		ctx.body.data.push({
			id: action.id,
			from,
			to,
			location,
			item,
			description: action.description,
			confirmed: action.confirmed,
			creation_date: action.creation_date
		});
	}

	pool.close();
	sql.close();
});
*/
export default router;

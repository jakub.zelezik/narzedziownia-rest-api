import * as Router from "koa-router";
import * as sql from "mssql";

import config from "../db"
import parseError from "../parseError";

import * as Joi from "joi";

import * as fs from "fs";
import * as jwt from "jsonwebtoken"
import * as path from "path";
import {ResponseBody, ResponseStatus} from "../interfaces";

const privateKey = fs.readFileSync(path.join(__dirname, './../../config/keys/jwtRS256.key'));

const router = new Router({ prefix: '/auth' })

router.post("/login", async (ctx) => {
	let { request: { body: { data } }} = ctx;

	console.log(ctx.request.body);

	const validator = Joi.object({
		password: Joi.string()
			.min(8)
			.max(256)
			.required(),
		email: Joi.string()
			.email()
			.required()
	});

	try {
		data = Joi.attempt(data, validator, { abortEarly: false });

		const { email, password } = data;

		const pool = await sql.connect(config);

		const { recordset: users } = await pool.request()
			.input('email', sql.NVarChar(50), email)
			.input('password', sql.NVarChar(256), password)
			.query(`
			select
				o.os_imie as name,
				o.os_nazwisko as surname,
				o.os_email as email,
				o.os_id as id,
				o.upr_id as perm_id
			from 
				osoby o
			where
				o.os_email = @email AND o.os_haslo = HASHBYTES('SHA2_256', @password);
		`);

		if(users.length){
			const [user] = users;
			console.log(`Zalogowano: ${user.id}`)

			const token : string = await jwt.sign({
				id: user.id,
				perm_id: user.perm_id,
				email: user.email,
				name: user.name,
				surname: user.surname
			}, privateKey, { algorithm: 'RS256', expiresIn: '24h' })

			ctx.status = 200;
			ctx.body = {
				status: ResponseStatus.ok,
				token
			} as ResponseBody;
		} else {
			ctx.status = 401;
			ctx.body = {
				status: ResponseStatus.authError,
				errors: [{
					field: null,
					error: 'Niepoprawny login lub hasło'
				}]
			} as ResponseBody;
		}
	} catch (e) {
		if(e.details instanceof Array){
			ctx.status = 406;
			ctx.body = {
				status: ResponseStatus.dataError,
				errors: parseError(e)
			} as ResponseBody;
		} else {
			console.log(e);
			ctx.status = 500;
			ctx.body = {
				status: ResponseStatus.internalError,
				errors: [{field: null, error: 'Internal Server Error'}]
			} as ResponseBody;
		}
	}
});
/*
router.post("/password", async (ctx) => {
	let { request: { body }} = ctx;

	const validator = Joi.object({
		auth_token: Joi.string()
			.length(50)
			.required(), 
		user_id: Joi.number()
			.min(1)
			.required(),
		password: Joi.string()
			.min(6)
			.max(50)
			.required()
	});

	try {
		body = Joi.attempt(body, validator, { abortEarly: false });
	} catch (e) {

		ctx.status = 300;

		ctx.body = parseError(e);

		return;
	}

	let { auth_token, user_id, password } = body;
	
	let pool = await sql.connect(config);

	let { recordset: users } = await pool.request()
		.input('auth_token', sql.NVarChar(50), auth_token)
		.input('user_id', sql.Int, user_id)
		.query(`
			select
				os_id as id
			from 
				osoby
			where
				os_token = @auth_token AND os_id = @user_id;
		`);

	if(users.length == 0) {
		ctx.status = 300;
	} else {
		await pool.request()
			.input('user_id', sql.Int, user_id)
			.input('password', sql.NVarChar(50), password)
			.query(`
				UPDATE 
					osoby  
				SET 
					os_haslo = HASHBYTES('SHA2_256', @password),
					os_token = NULL
				WHERE 
					os_id = @user_id;
			`);

			ctx.status = 200;
			ctx.body = {
				massage: 'password set'
			}
	}

	pool.close();
});*/

export default router;

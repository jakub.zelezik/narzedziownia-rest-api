import * as sql from "mssql";

enum FilterType {
    equal = '=',
    grater = '>',
    smaller = '<'
}

export default class QueryFilter {
    private query_text = '';

    private rawSqlArray: string[] = [];

    private args: {name: string, value: any, type: sql.TypeObject}[] = [];

    private customFilters: {type: string, generator: (dict: any, item: {field: string, type: FilterType, value: string | number }) => string}[] = [];

    public query(query: string): QueryFilter {
        this.query_text = query;
        return this;
    }

    public addCustomFilter(type: string, generator: (dict: any, item: {field: string, type: FilterType | string, value: string | number }) => string): QueryFilter {
        this.customFilters.push({
            type,
            generator
        });

        return this;
    }

    public filter(base64String: string, dict: any): QueryFilter {
        try{
            const data: {field: string, type: FilterType | string, value: string | number }[] = JSON.parse(new Buffer(decodeURIComponent(base64String), 'base64').toString('ascii'));

            for(const item of data) {
                if(dict[item.field] != undefined) {
                    if(item.type == '=' || item.type == '>' || item.type == '<'){
                        this.rawSqlArray.push(`${dict[item.field]} ${item.type} @${item.field}_filter`);
                    } else {
                        const filter: {type: string, generator: (dict: any, item: {field: string, type: FilterType | string, value: string | number }) => string} | null = this.customFilters.find(e => e.type == item.type);

                        if(filter != null) {
                            this.rawSqlArray.push(filter.generator(dict, item));
                        }
                    }
                    this.args.push({
                        name: `${item.field}_filter`,
                        value: item.value,
                        type: (typeof item.value === "string") ? sql.NVarChar(50) : sql.Int
                    });
                }
            }


        } catch (e) {
            console.log(e);
        }

        return this;
    }

    public input(name: string, value: any, type: sql.TypeObject): QueryFilter {
        this.args.push({
            name: name,
            value: value,
            type: type
        });

        return this;
    }

    public async call(sql_handler: sql.RequestHandler): Promise<sql.QueryObjectsReturnType> {
        for(const arg of this.args) {
            sql_handler.input(arg.name, arg.type, arg.value);
        }

        if(this.rawSqlArray.length == 0){
            return await sql_handler.query(this.query_text.replace('%%FILTER_RULES%%','1 = 1'));
        } else {
            return await sql_handler.query(this.query_text.replace('%%FILTER_RULES%%', this.rawSqlArray.join(' AND ')));
        }
    }
}

import {ErrorRecord} from "./interfaces";

const parseError = (error) : ErrorRecord[] => {
	let result = [];

	for(const item of error.details) {
		result.push({
			field: item.context.label,
			error: item.message
		});
	}

	return result;
}

export default parseError;
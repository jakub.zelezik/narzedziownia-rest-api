export interface DataId {
  id: number;
}

export interface NewLocationData {
  name: string;
  type_id: number;
  street: string;
  post_code: string;
  city: string;
}

export interface LocationData extends DataId {
  name: string;
  type: string;
  street: string;
  post_code: string;
  city: string;
}

export interface NewItemData {
  name: string;
  part_number: string;
  serial_number: string;
  description: string;
  location_id: number;
}

export interface ItemData extends DataId {
  name: string;
  part_number: string;
  serial_number: string;
  description?: string;
  creation_date: string;
}

export interface NewUserData {
  name: string;
  surname: string;
  email: string;
  perm_id: number;
  password: string;
}

export interface UserData extends DataId {
  name: string;
  surname: string;
  email: string;
  perm: string;
}

export enum LoadingState {
  loading = 0,
  loaded = 1,
  error = 2,
}

export interface NewActionData {
  from?: number;
  to?: number;
  location: number;
  item: number;
  description: string;
}

export interface ActionData extends DataId {
  from?: UserData;
  to?: UserData;
  location: LocationData;
  item: ItemData;
  description: string;
  confirmed: boolean;
  creation_date: Date;
}

export interface InputListItem {
  id: number;
  display_name: string;
}

export enum ResponseStatus {
  ok = 'ok',
  authError = 'authError',
  dataError = 'dataError',
  internalError = 'internalError',
}

export interface RequestBody {
  token?: string;
  data?: any;
}

export interface ErrorRecord {
  field: string | null;
  error: string;
}

export interface ResponseBody {
  status: ResponseStatus;
  data?: any;
  errors?: ErrorRecord[];
}

import * as fs from "fs";
import * as jwt from "jsonwebtoken"
import * as path from "path";
const publicKey = fs.readFileSync(path.join(__dirname, './../config/keys/jwtRS256.key.pub'));

async function verifyToken(tokenString: string | null | undefined){
    if (tokenString) {
        try {
            const token = tokenString.split(' ')[1];
            return jwt.verify(token, publicKey);
        } catch (err) {
            return null;
        }
    }
    return null;
}

export default verifyToken

import * as fs from 'fs';
import * as path from 'path';
import * as https from 'https';

import app from './app'

const https_config = {
	domain: 'backend.sal.test-project-domain.com',
	https: {
		port: 443,
		options: {
			key: fs.readFileSync(path.resolve('/etc/letsencrypt/live/backend.sal.test-project-domain.com/privkey.pem'), 'utf8').toString(),
			cert: fs.readFileSync(path.resolve('/etc/letsencrypt/live/backend.sal.test-project-domain.com/fullchain.pem'), 'utf8').toString()
		}
	}
};

const httpsServer = https.createServer(https_config.https.options, app.callback());

httpsServer.listen(https_config.https.port, () => {
	console.log("Koa started");
});